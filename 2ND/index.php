<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        label {
            display: block
        }
    </style>
</head>

<body>
    <form action="welcome.php" method="post">
        <fieldset>
            <legend>Personal Information</legend>
            <label for="userName">User Name</label>
            <input type="text" name="user_name" placeholder="type your user name">
            <label for="password">Password</label>
            <input type="password" name="password" placeholder="type your user password">
            <label for="Email">E-Mail</label>
            <input type="email" name="email" placeholder="example@mail.com">
            <label for="phone">Phone NO.</label>
            <input type="tel" name="phone_number">
            <label for="webSite">WebSite</label>
            <input type="url" name="website" placeholder="http://example.com">
            <label for="photo">Profile Picture</label>
            <input type="file" name="photo">
            <label for="gender">Gender</label>
            <select name="gender">
                <option value="male">Male</option>
                <option value="female">Female</option>
            </select>
        </fieldset>
        <fieldset>
            <legend>choose your programing language</legend>
            <input type="radio" name="programing_language" value="PHP"> PHP
            <input type="radio" name="programing_language" value="Paython"> Paython
            <input type="radio" name="programing_language" value="#C"> #C
        </fieldset>
        <fieldset>
            <legend>Choose your operating system</legend>
            <input type="checkbox" name="operating_system[]" value="MAC"> MAC
            <input type="checkbox" name="operating_system[]" value="Linux"> Linux
            <input type="checkbox" name="operating_system[]" value="Windows"> Windows
        </fieldset>
        <input type="submit" name="form">
        <input type="reset">
    </form>
</body>

</html>