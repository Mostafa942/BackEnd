<?php

$message = [];

$errors = [];
// validate User Name=>
if (!empty($_POST['form']) && empty($_POST['user_name'])) {
    $errors[] = 'please enter your user name';
} elseif (!preg_match('/^[A-Z][a-z]+$/', $_POST['user_name'])) {
    $errors[] = 'user name should start by capital letter';
} else {
    $message[] = 'your user name is : ' . $_POST['user_name'] . '';
}
// validate password=>
if (!empty($_POST['form']) && empty($_POST['password'])) {
    $errors[] = 'please enter your password';
} elseif (!preg_match('/^\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[\d])\S*$/', $_POST['password'])) {
    $errors[] = 'not less than 8 characters';
}
// validate email=>
if (!empty($_POST['form']) && empty($_POST['email'])) {
    $errors[] = 'please enter your email';
} elseif (!preg_match('/^[a-z0-9-_.]+@gmail.com$/', $_POST['email'])) {
    $errors[] = 'your email isn\'t correct';
} else {
    $message[] = 'your email is :'.$_POST['email'].'';
}
// validate phone NO.=>
if (!empty($_POST['form']) && empty($_POST['phone_number'])) {
    $errors[] = 'please enter your phone_number';
} elseif (!preg_match('/^\+[0-9]+[0-9{11,11}]$/', $_POST['phone_number'])) {
    $errors[] = 'your phone must begain by country code';
} else {
    $message[] = 'your phone number is :'.$_POST['phone_number'].'';
}
// validate website=>
if (!empty($_POST['form']) && empty($_POST['website'])) {
    $errors[] = 'please enter your website';
} elseif (!preg_match('/^http:\/\/www\.[a-z]+\.com$/', $_POST['website'])){
    $errors[] = 'your website isn\'t correct';
} else {
    $message[] = 'your website is '.$_POST['website'].'';
}
// validate photo=>
if (!empty($_POST['form']) && empty($_POST['photo'])) {
    $errors[] = 'please choose your photo';
} else {
    $message[] = '<img src="'.$_POST['photo'].'">' ;
}
// validate gender=>
if (isset($_POST['gender'])) {
    $message[] = 'your gender is '.$_POST['gender'].'';
}
// validate programing_language=>
if (!empty($_POST['form']) && empty($_POST['programing_language'])) {
    $errors[] = 'please chose your programing language';
} else {
    $message[] = 'your programing language is : '.$_POST['programing_language'].'';
}
if (!empty($_POST['form']) && empty($_POST['operating_system'])) {
    $errors[] = 'please chose your operating system';
} else {
    for ($o = 0; $o < count($_POST['operating_system']); $o++) {
        $message[] = 'your operating system is :'.$_POST['operating_system'][$o].'';
    }
}

if (count($errors)) :
?>
    <ul>
        <?php
        for ($i = 0; $i < count($errors); $i++) :
        ?>
            <li>
                <?= $errors[$i] ?>
            </li>
        <?php
        endfor;
        ?>
    </ul>
<?php
endif;
?>



<h1>
    <?php
    if (count($message)) {
    ?>
        <ul>
            <?php
            for ($n = 0; $n < count($message); $n++) {
            ?>
                <li>
                    <?= $message[$n] ?>
                </li>
            <?php
            }
            ?>
        </ul>
    <?php
    }
    ?>
</h1>