<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        form {
            text-align: center;
        }
        label {
            display: block;
            color: red;
        }
        button {
            display: block;
            margin: 10px auto
        }
    </style>
</head>
<body>
    <form method="get" action="register.php">
        <label for="name">Name</label>
        <input type="text" name="name" required>
        <label for="email">Email</label>
        <input type="text" name="email" required>
        <label for="password">Password</label>
        <input type="password" name="password" required>
        <label for="repasword">Re-Password</label>
        <input type="password" name="repass" required>
        <label for="gender">Gender</label>
        <select name="gender" required>
            <option value="male">Male</option>
            <option value="female">Female</option>
        </select>
        <button type="submit">Register</button>
    </form>
</body>
</html>