<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        form {
            text-align: center;
        }
        label {
            color: red;
            display: block;
        }
        button {
            display: block;
            margin: 10px auto 0;
        }
    </style>
</head>
<body>
    <form method="get" action="loged.php">
        <label for="email">Email</label>
        <input type="text" placeholder="type email" name="email">
        <label for="password">Password</label>
        <input type="password" placeholder="type password" name="password" required>
        <button type="submit">Login</button>
    </form>
</body>
</html>